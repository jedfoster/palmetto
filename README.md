# Palmetto Code Challenge 

## Delivery Notes

**Live demo:** [https://palmetto.drft.io/](https://palmetto.drft.io/)

I would have liked to get in some of the strech goal items, but I ran out of time. 

If I were to spend another half hour or so, the next item I would fix is the ordering of the saved items list. Right now, because I'm using an object with the photo IDs as keys, the last item the user saved is not appended to the end of the list, but where ever it falls in the ascending order of the keys. Not ideal.

--- 


## Criteria

- [x] Replicate the mockup (see below)
- [x] The component should at least display the first 10 search results
- [x] The search should be case insensitive and trim whitespace
- [x] The search also features a category filter which should happen on the request
- [x] Allow each search result to be “saved” by the user. Display the saved items as links in their own section

### Stretch Goals

The original criteria were vague on the following points, so I'm classing them as "nice-to-haves".

- [ ] Pagination. Display some sort of pagination control/navigation
- [ ] Unsave. Should be able to "unsave" a saved item.
- [ ] Additional search params. e.g. `orientation`, `colors`, `editors_choice`, `order`
- [ ] Form value persistence. Perhaps save and restore keyword/category value with SessionStorage, or even save the actual search results so that refreshing the page doesn't lose your results. 
- [ ] Clear form? Search result persistence might also necessitate a form reset control.


## Notes

1. We encourage the use of create-react-app, but feel free to use what you feel most comfortable with (codepen, jsfiddle, etc)
2. API: https://pixabay.com/api/docs/
3. API Key: 13136421-266c28a6d61717bc2e4e6a83e (if expired, please renew:
pixabay.com/accounts/register)
4. Categories: fashion, nature, backgrounds, science, education, people,
feelings, religion, health, places, animals, industry, food, computer,
sports, transportation, travel, buildings, business, music
5. You are encouraged to use any framework and/or libraries you think will allow you to show us your best work. We value working, readable, and maintainable solutions over algorithmic perfection
6. When you’re done, send us your solution either in an archive or as a link to a repository

## Mockup

![mockup](./mockup.png)

---


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
