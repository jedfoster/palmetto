import './SearchResults.css';

function SearchResults({ items, savedItems, handleSaveItem }) {
  if (!items) {
    return (
      <div>
        <p>
          <strong>Search for an image using the form above.</strong>
        </p>
      </div>
    );
  } else if (items.length < 1) {
    return (
      <div>
        <p>
          <strong>Sorry, we couldn't find any matches.</strong>
        </p>

        <p>Things you can try:</p>

        <ul>
          <li>Check the spelling.</li>
          <li>Use synonyms or generic terms.</li>
        </ul>
      </div>
    );
  }

  const results = items.map(({
    id,
    previewURL,
    largeImageURL,
    tags,
    favorites,
    likes,
    pageURL,
    user
  }) => (
    <article className="search-result-item" key={id}>
      <figure data-saved={savedItems && !!savedItems[id]} onClick={(e) => {
        e.currentTarget.dataset.saved = true;
        handleSaveItem({ id, largeImageURL });
      }}>
        <img src={previewURL} alt={tags} />
      </figure>

      <ul className="tags">
        {
          tags.split(', ').map((tag) => (
            <li key={tag}>
              <span>{tag}</span>
            </li>
          ))
        }
      </ul>

      <div className="stats">
        <span className="likes">{likes}</span>
        <span className="favorites">{favorites}</span>
      </div>
    </article>
  ));

  return (
    <section className="container">
      { results }
    </section>
  )
}

export default SearchResults;
