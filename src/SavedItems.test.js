/* eslint-disable no-unused-vars */
import { render, screen } from '@testing-library/react';
import SavedItems from './SavedItems';

describe('SavedItems', () => {
  test('displays nothing if no saved items', () => {
    const { container } = render(<SavedItems />);
    // screen.debug(container)

    expect(container.querySelector('ul')).toBeEmptyDOMElement();
  });

  test('displays results', () => {
    const mockSavedItems = [
      [ '12345', 'https://cdn.pixabay.com/photo/12345.jpg' ],
      [ '45678', 'https://cdn.pixabay.com/photo/45678.jpg' ],
      [ '90123', 'https://cdn.pixabay.com/photo/90123.jpg' ],
    ];

    const { container } = render(<SavedItems items={mockSavedItems} />);
    // screen.debug(container)

    expect(container.querySelector('li:nth-child(3) a').href).toBe('https://cdn.pixabay.com/photo/90123.jpg')
  });
});
