/* eslint-disable no-unused-vars */
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import search from './lib/search';
import App from './App';

jest.mock('./lib/search')

describe('App', () => {
  test('renders learn react link', () => {
    const { container } = render(<App />);
    // screen.debug(container)

    const promptText = screen.getByText(/form above/i);

    expect(promptText).toBeInTheDocument();
  });

  test('handleSearch', async () => {
    const { container } = render(<App />);
    // screen.debug(container.firstChild)

    search.mockResolvedValue({ hits: [] });

    fireEvent.submit(container.querySelector('form'));

    await waitFor(() => {
      expect(search).toHaveBeenCalled()
    });
  });
});
