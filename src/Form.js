import './Form.css';
import { useState } from 'react';

const categories = process.env.REACT_APP_API_CATEGORIES.split(', ');

function Form({ keyword: keywordProp, category: categoryProp, handleSubmit: handleSubmitProp }) {

  const categoryOptions = categories.map((val) => (<option value={val} key={val}>{val}</option>));

  const [keyword, setKeyword] = useState(keywordProp);
  const [category, setCategory] = useState(categoryProp || '');

  const handleSubmit = (e) => {
    e.preventDefault();

    handleSubmitProp({ keyword, category });
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        aria-label="keyword-input"
        type="search"
        placeholder="Keyword..."
        defaultValue={keyword}
        onChange={(e) => setKeyword(e.target.value)}
        className="u-full-width"
      />

      <select
        aria-label="category-input"
        defaultValue={category}
        onChange={(e) => setCategory(e.target.value)}
        className={`u-full-width ${!category ? 'isDefault' : ''}`}
      >
        <option default disabled value="">Category...</option>
        {
          categoryOptions
        }
      </select>

      <button className="search button-primary u-full-width">Search</button>
    </form>
  );
}

export default Form;
