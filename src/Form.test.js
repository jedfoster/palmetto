/* eslint-disable no-unused-vars */
import { render, screen, fireEvent } from '@testing-library/react';
import Form from './Form';

describe('Form', () => {
  test('renders search form', () => {
    const { container } = render(<Form />);
    // screen.debug(container)

    const keywordInput = screen.getByLabelText('keyword-input');
    const categoryInput = screen.getByLabelText('category-input');
    const searchButton = screen.getByText('Search');

    expect(keywordInput).toBeInTheDocument();
    expect(categoryInput).toBeInTheDocument();
    expect(searchButton).toBeInTheDocument();
  });

  test('accepts keyword and category props', () => {
    const { container } = render(<Form keyword="fuzzy" category="animals" />);
    // screen.debug(container)

    const keywordInput = screen.getByLabelText('keyword-input');
    const categoryInput = screen.getByLabelText('category-input');

    expect(keywordInput.value).toBe('fuzzy');
    expect(categoryInput.value).toBe('animals');
  });

  test('allows making a category selection', () => {
    const { container } = render(<Form keyword="fuzzy" category="animals" />);
    // screen.debug(container)

    const keywordInput = screen.getByLabelText('keyword-input');
    const categoryInput = screen.getByLabelText('category-input');

    fireEvent.change(categoryInput, { target: { value: 'places' } });

    expect(categoryInput.value).toBe('places');
  });

  test('handles form submission', () => {
    const onSubmit = jest.fn();
    const { container } = render(<Form handleSubmit={onSubmit} />);
    // screen.debug(container.firstChild)

    fireEvent.submit(container.firstChild);
    expect(onSubmit).toHaveBeenCalled();

  });
});
