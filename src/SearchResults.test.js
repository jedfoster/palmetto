/* eslint-disable no-unused-vars */
import { render, screen, fireEvent } from '@testing-library/react';
import SearchResults from './SearchResults';

import mockSearchResults from './__tests__/mockSearchResults.json';

describe('SearchResults', () => {
  test('displays a prompt if no results are provided', () => {
    const { container } = render(<SearchResults items={null} />);
    // screen.debug(container)

    const promptText = screen.getByText(/form above/i);

    expect(promptText).toBeInTheDocument();
  });

  test('displays "Sorry, no matches" if result is an empty array', () => {
    const { container } = render(<SearchResults items={[]} />);
    // screen.debug(container)

    const promptText = screen.getByText(/couldn't find any matches/i);

    expect(promptText).toBeInTheDocument();
  });

  test('displays results', () => {
    const { container } = render(<SearchResults items={mockSearchResults} />);
    // screen.debug(container)

    expect(container.querySelector('article:nth-child(3) img').src).toBe('https://cdn.pixabay.com/photo/2016/09/05/21/37/cat-1647775_150.jpg')
  });

  test('marks saved items as SAVED', () => {
    const mockSavedItems = {
      '551554': 'https://pixabay.com/get/53e5d4464f56b108f5d084609629347f1639dae7524c704f75287fd49249c459_1280.jpg',
      '2948404': 'https://pixabay.com/get/54e9d14b4e52a814f6da8c7dda79367d143cd8e151506c48732f7cd09748c159b9_1280.jpg'
    };

    const { container } = render(
      <SearchResults items={mockSearchResults} savedItems={mockSavedItems} />
    );
    // screen.debug(container)

    expect(container.querySelector('article:nth-child(4) figure').dataset.saved).toBe('true')
  });

  test('clicking on an item invokes handleSaveItem', () => {
    const mockHandleSaveItem = jest.fn();
    const { container } = render(
      <SearchResults items={mockSearchResults} handleSaveItem={mockHandleSaveItem} />
    );
    // screen.debug(container)

    fireEvent.click(container.querySelector('article:nth-child(3) figure'));
    expect(mockHandleSaveItem).toBeCalledWith({
      id: 1647775,
      largeImageURL: 'https://pixabay.com/get/57e6d1444d55a914f6da8c7dda79367d143cd8e151506c48732f7cd09748c159b9_1280.jpg'
    })
  });

});
