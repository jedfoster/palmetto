import './SavedItems.css';

function SavedItems({ items }) {
  const savedItems = items ? items.map(([ id, url ]) => (
    <li key={id}>
      <a href={url} rel="external" target="_blank">#{id}</a>
    </li>
  )) : [];

  return (
    <div className="saved-items four columns">
      <h5>Saved</h5>

      <ul>
        { savedItems }
      </ul>
    </div>
  );
}

export default SavedItems;
