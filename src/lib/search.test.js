import search from './search';
import { get } from './fetch';

jest.mock('./fetch');

describe('search', () => {
  test('builds a proper query URL', async () => {
    const result = await search('kitten', 'animals');

    expect(get.mock.calls[0].toString()).toBe('https://pixabay.com/api/?key=13136421-266c28a6d61717bc2e4e6a83e&category=animals&q=kitten&safesearch=true&image_type=photo');
  });
});
