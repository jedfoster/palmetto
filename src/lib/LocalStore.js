const LocalStore = {
  get(key) {
    const value = localStorage.getItem(key);

    try {
      return JSON.parse(value);
    } catch (e) {
      return value;
    }
  },

  set(key, valueArg) {
    const value = typeof valueArg !== 'string' ? JSON.stringify(valueArg) : valueArg;

    localStorage.setItem(key, value);
  },

  delete(key) {
    localStorage.removeItem(key);
  }
};

export default LocalStore;
