import { get } from './fetch';

export default async function search(keyword, category) {
  const key = process.env.REACT_APP_API_KEY;
  const url = new URL(process.env.REACT_APP_API_BASE_URL);

  const params = {
    key,
    category,
    q: keyword.toLowerCase().trim(),
    safesearch: true,
    image_type: 'photo'
  };

  url.search = new URLSearchParams(params).toString();

  return await get(url);
}
