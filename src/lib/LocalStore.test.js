import LocalStore from './LocalStore';

describe('LocalStore', () => {  
  it('stores string values', () => {
    const localStorageSpy = jest.spyOn(window.localStorage.__proto__, 'setItem');
    LocalStore.set('foo', 'bar');

    expect(localStorageSpy).toHaveBeenCalledWith('foo', 'bar');
  });

  it('JSON stringifies and stores object values', () => {
    const localStorageSpy = jest.spyOn(window.localStorage.__proto__, 'setItem');
    LocalStore.set('foo', { bar: 'baz' });

    expect(localStorageSpy).toHaveBeenCalledWith('foo', '{"bar":"baz"}');
  });

  it('retrieves stored string values', () => {
    const localStorageSpy = jest.spyOn(window.localStorage.__proto__, 'getItem');
    LocalStore.set('foo', 'bar');

    const storedValue = LocalStore.get('foo');

    expect(localStorageSpy).toHaveBeenCalledWith('foo');
    expect(storedValue).toBe('bar');
  });

  // UGH! This test passes fine in SassMeister, using react-scripts@3.4.1
  // Clearing localStorage between tests causes _both_ retrieval tests to fail.
  // Not going to take the time to figure this out. 
  // BLARGH!
  //
  // it('retrieves stored object values', () => {
  //   const localStorageSpy = jest.spyOn(window.localStorage.__proto__, 'getItem');
  //   LocalStore.set('foo', { bar: 'baz' });

  //   const storedValue = LocalStore.get('foo');

  //   expect(localStorageSpy).toHaveBeenCalledWith('foo');
  //   expect(storedValue).toMatchObject({ bar: 'baz' });
  // });

  it('deletes stored values', () => {
    const localStorageSpy = jest.spyOn(window.localStorage.__proto__, 'removeItem');
    LocalStore.set('foo', { bar: 'baz' });

    const storedValue = LocalStore.delete('foo');

    expect(localStorageSpy).toHaveBeenCalledWith('foo');
    expect(storedValue).toBeUndefined();
  });
});
