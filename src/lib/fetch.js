export async function get(url, headers) {
  const method = 'GET';
  try {
    const response = await fetch(url, { method, headers });
    const json = await response.json();

    return json;
  } catch (error) {
    return error;
  }
}

export async function post(url, data, customHeaders) {
  const method = 'POST';
  const headers = { 'Content-Type': 'application/json', ...customHeaders };
  const body = JSON.stringify(data);

  try {
    const response = await fetch(url, { method, headers, body });
    const json = await response.json();

    return json;
  } catch (error) {
    return error;
  }
}
