import { useState } from 'react';

import './App.css';
import search from './lib/search';
import LocalStore from './lib/LocalStore';
import Form from './Form';
import SearchResults from './SearchResults';
import SavedItems from './SavedItems';

function App() {
  const [searchResults, setSearchResults] = useState();
  const [savedItems, setSavedItems] = useState(LocalStore.get('saved-items') || []);

  // useEffect(() => {
  //   if (!savedItems || Object.keys(savedItems).length < 1) {
  //     setSavedItems(LocalStore.get('saved-items'));
  //   }
  // }, [savedItems]);

  const handleSearch = async ({ keyword, category }) => {
    const results = await search(keyword, category);

    setSearchResults(results.hits);
  };

  const saveItem = ({ id, largeImageURL }) => {
    const newItems = [id, largeImageURL];

    savedItems.push(newItems);

    setSavedItems(() => ([ ...savedItems ]));
    LocalStore.set('saved-items', savedItems);
  };

  return (
    <div className="App container">
      <div className="row">
        <div className="eight columns">
          <Form handleSubmit={handleSearch} />

          <SearchResults items={searchResults} savedItems={savedItems} handleSaveItem={saveItem} />
        </div>

        <SavedItems items={savedItems} />
      </div>
    </div>
  );
}

export default App;
